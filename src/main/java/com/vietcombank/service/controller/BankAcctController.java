package com.vietcombank.service.controller;

import com.vietcombank.service.entity.BankAcctModel;
import com.vietcombank.service.entity.TransactionModel;
import com.vietcombank.service.service.BankAcctService;
import com.vietcombank.service.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("restapi")
@RequiredArgsConstructor
public class BankAcctController {
    @Autowired
    private BankAcctService bankAcctService;

    @GetMapping(value = "/v1/bankacct/getall", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(bankAcctService.GetAll());
    }

    @GetMapping(value = "/v1/bankacct/getAllWithCifNo", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getAllWithCifNo(@RequestBody String cifNo) {
        return ResponseEntity.ok(bankAcctService.getAllWithCifNo(cifNo));
    }

    @GetMapping(value = "/v1/bankacct/getDetail", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> getDetail(@RequestBody String cifNo, String accNo) {
        return ResponseEntity.ok(bankAcctService.getDetail(cifNo, accNo));
    }

    @PutMapping(value = "/v1/bankacct/close", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> close(@RequestBody UUID id) {
        return ResponseEntity.ok(bankAcctService.close(id));
    }

    @PutMapping(value = "/v1/bankacct/topup", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> topup(@RequestBody float amount, String accNo) {
        return ResponseEntity.ok(bankAcctService.topup(amount,accNo));
    }

    @PutMapping(value = "/v1/bankacct/withdraw", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> withdraw(@RequestBody float amount, String accNo) {
        return ResponseEntity.ok(bankAcctService.topup(amount,accNo));
    }

    @PutMapping(value = "/v1/bankacct/create", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> create(@RequestBody BankAcctModel model) {
        return ResponseEntity.ok(bankAcctService.create(model));
    }
}
