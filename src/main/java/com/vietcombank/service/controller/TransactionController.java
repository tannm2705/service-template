package com.vietcombank.service.controller;

import com.vietcombank.service.entity.TransactionModel;
import com.vietcombank.service.service.TransactionService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("restapi")
@RequiredArgsConstructor

public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    @GetMapping(value = "/v1/transaction/getall", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public List<TransactionModel> getAll() {
        return transactionService.GetAll();
    }

    @PostMapping(value = "/v1/transaction/create", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> create(@RequestBody TransactionModel model) {
        return ResponseEntity.ok(transactionService.cteateTransaction(model));
    }

    @PutMapping(value = "/v1/transaction/update", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> update(@RequestBody TransactionModel model) {
        return ResponseEntity.ok(transactionService.updateTransaction(model));
    }

}