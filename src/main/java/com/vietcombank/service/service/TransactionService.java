package com.vietcombank.service.service;

import com.vietcombank.service.entity.TransactionModel;
import com.vietcombank.service.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor // create a constructor automatically based on the params during compile time
@Transactional
public class TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;
    public List<TransactionModel> GetAll() {
        return transactionRepository.findAll();
    }

    public TransactionModel cteateTransaction(TransactionModel model) {
        return transactionRepository.save(model);
    }

    public TransactionModel updateTransaction(TransactionModel model) {
        TransactionModel item = transactionRepository.getById(model.getId());
        item.setAmount(model.getAmount());
        item.setBankAcct(model.getBankAcct());
        item.setCustCif(model.getCustCif());
        item.setSavingAcct(model.getSavingAcct());
        return transactionRepository.save(item);
    }

    public String deleteTransaction(UUID id) {
        TransactionModel item = transactionRepository.getById(id);
        transactionRepository.delete(item);
        return "Success";
    }
    public TransactionModel createTransaction(TransactionModel model) {
        return transactionRepository.save(model);
    }
}