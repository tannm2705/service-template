package com.vietcombank.service.service;

import com.vietcombank.service.entity.SampleTableEntity;
import com.vietcombank.service.repository.SampleTableEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SampleTableService {
    @Autowired
    private SampleTableEntityRepository sampleTableEntityRepository;

    public List<SampleTableEntity> retrieveAll() {
        return sampleTableEntityRepository.findAll();
    }
}
