package com.vietcombank.service.service;

import com.vietcombank.service.entity.BankAcctModel;
import com.vietcombank.service.entity.TransactionModel;
import com.vietcombank.service.repository.BankAcctRepository;
import com.vietcombank.service.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.UUID;


@Service
@RequiredArgsConstructor // create a constructor automatically based on the params during compile time
@Transactional
public class BankAcctService {
    @Autowired
    private BankAcctRepository bankAcctRepository;

    public List<BankAcctModel> GetAll() {
        return bankAcctRepository.findAll();
    }

    public List<BankAcctModel> getAllWithCifNo(String cifNo) {
        return bankAcctRepository.findByCustomerCifNo(cifNo);
    }

    public BankAcctModel getDetail(String cifNo, String bankAccNo) {
        return bankAcctRepository.findAllByCustomerCifNoAndAccountNo(cifNo, bankAccNo);
    }

    public String close(UUID id) {
        BankAcctModel item = bankAcctRepository.getReferenceById(id);
        item.setStatus("CLOSED");
        item.setClosedDate(Instant.now());
        bankAcctRepository.save(item);
        return "SUCCESS";
    }

    public String topup(float amount, String bankAccNo) {
        BankAcctModel item = bankAcctRepository.findBankAcctModelByAccountNo(bankAccNo);
        item.setBalance(item.getBalance() + amount);
        bankAcctRepository.save(item);
        return "SUCCESS";
    }

    public String withdraw(float amount, String bankAccNo) {
        BankAcctModel item = bankAcctRepository.findBankAcctModelByAccountNo(bankAccNo);
        item.setBalance(item.getBalance() - amount);
        bankAcctRepository.save(item);
        return "SUCCESS";
    }

    public BankAcctModel create(BankAcctModel model) {
        return bankAcctRepository.save(model);
    }
}
