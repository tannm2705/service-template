package com.vietcombank.service.repository;

import com.vietcombank.service.entity.BankAcctModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface BankAcctRepository extends JpaRepository<BankAcctModel, UUID> {
        List<BankAcctModel> findByCustomerCifNo(String cifNo);

        BankAcctModel findAllByCustomerCifNoAndAccountNo(String cifNo, String acctNo);

        BankAcctModel findBankAcctModelByAccountNo(String acctNo);
}