package com.vietcombank.service.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "BANK_ACCT_TABLE")

public class BankAcctModel {
    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "account_no")
    private String accountNo;
    @Column(name = "balance")
    private float   balance;
    @Column(name = "created_date")
    private Instant createdDate;
    @Column(name = "closed_date")
    private Instant closedDate;
    @Column(name = "type")
    private String type;
    @Column(name = "status")
    private String status;
    @Column(name = "customer_cif_no")
    private String customerCifNo;

    public UUID getId() { return id;}
    public void setId(UUID id) {this.id  = id;}
    public String getAccountNo() {return accountNo;}
    public void setAccountNo(String accountNo){this.accountNo = accountNo;}
    public float getBalance() {return balance;}
    public void setBalance(float balance){this.balance = balance;}
    public Instant getCreatedDate() {return createdDate;}
    public void setCreatedDate(Instant createdDate) {this.createdDate = createdDate;}
    public Instant getClosedDate() {return closedDate;}
    public void setClosedDate(Instant closedDate) {this.closedDate = closedDate;}
    public String getType() {return type;}
    public void setType(String type) {this.type = type;}
    public String getStatus() {return status;}
    public void setStatus(String status) {this.status = status;}
    public String getCustomerCifNo() {return customerCifNo;}
    public void setCustomerCifNo(String customerCifNo) {this.customerCifNo = customerCifNo;}
}
