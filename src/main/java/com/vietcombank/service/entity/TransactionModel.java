package com.vietcombank.service.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import javax.xml.transform.sax.SAXResult;
import java.util.UUID;

@Entity
@Table(name = "TRANSACTION_TABLE")
public class TransactionModel {
    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "amount")
    private float amount;
    @Column(name = "bank_account_no")
    private String bank_account_no;
    @Column(name = "customer_cif_no")
    private String customer_cif_no;
    @Column(name = "saving_account_no")
    private String saving_account_no;
    @Column(name = "type")
    private String type;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    public float getAmount() {
        return amount;
    }
    public void setAmount(float amount) {
        this.amount = amount;
    }


    public String getBankAcct() {
        return bank_account_no;
    }
    public void setBankAcct(String bank_account_no) {
        this.bank_account_no = bank_account_no;
    }
    public String getCustCif() {
        return customer_cif_no;
    }
    public void setCustCif(String customer_cif_no) {
        this.customer_cif_no = customer_cif_no;
    }

    public String getSavingAcct() {
        return saving_account_no;
    }
    public void setSavingAcct(String saving_account_no) {
        this.saving_account_no = saving_account_no;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
