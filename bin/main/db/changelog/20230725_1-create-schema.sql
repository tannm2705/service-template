--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE SAMPLE_TABLE
(
    ID         uuid PRIMARY KEY default uuid_generate_v4(),
    PRODUCT    varchar(255),
    UPDATED_AT timestamp
);