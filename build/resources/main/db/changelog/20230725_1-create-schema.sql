--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE SAMPLE_TABLE
(
    ID         uuid PRIMARY KEY default uuid_generate_v4(),
    PRODUCT    varchar(255),
    UPDATED_AT timestamp
);

CREATE TABLE TRANSACTION_TABLE
(
    id         uuid PRIMARY KEY default uuid_generate_v4(),
    amount    DECIMAL(19,2),
    bank_account_no varchar(255),
    customer_cif_no varchar(255),
    saving_account_no varchar(255),
    type varchar(255)
);


CREATE TABLE BANK_ACCT_TABLE
(
    id         uuid PRIMARY KEY default uuid_generate_v4(),
    account_no       varchar(255),
    balance         decimal(19,0),
    created_date    date,
    closed_date     date,
    type            varchar(255),
    status          varchar(10),
    customer_cif_no   varchar(50)
);